# backup of node_modules for the Gulp-setup of Compony

Version installed on 03/9/2019
size: 142MB installed through npm
size: 143MB installed through yarn

nvm --version
0.33.11

npm --version
6.10.2

node --version
v10.16.0

Yarn --version
1.17.3

Tested on:
 - compony-gulp v1.2.15

Current issues:
 - Still 1 low severity vulnerabilities
 - Still 8 packages outdated
